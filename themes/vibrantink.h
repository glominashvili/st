/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#878787", /* black   */
	[1] = "#ff6600", /* red     */
	[2] = "#ccff04", /* green   */
	[3] = "#ffcc00", /* yellow  */
	[4] = "#44b4cc", /* blue    */
	[5] = "#9933cc", /* magenta */
	[6] = "#44b4cc", /* cyan    */
	[7] = "#f5f5f5", /* white   */

	/* 8 bright colors */
	[8]  = "#555555", /* black   */
	[9]  = "#ff0000", /* red     */
	[10] = "#00ff00", /* green   */
	[11] = "#ffff00", /* yellow  */
	[12] = "#0000ff", /* blue    */
	[13] = "#ff00ff", /* magenta */
	[14] = "#00ffff", /* cyan    */
	[15] = "#e5e5e5", /* white   */

	/* special colors */
	[256] = "#000000", /* background */
	[257] = "#ffffff", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
