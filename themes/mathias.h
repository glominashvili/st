/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#000000", /* black   */
	[1] = "#e52222", /* red     */
	[2] = "#a6e32d", /* green   */
	[3] = "#fc951e", /* yellow  */
	[4] = "#c48dff", /* blue    */
	[5] = "#fa2573", /* magenta */
	[6] = "#67d9f0", /* cyan    */
	[7] = "#f2f2f2", /* white   */

	/* 8 bright colors */
	[8]  = "#555555", /* black   */
	[9]  = "#ff5555", /* red     */
	[10] = "#55ff55", /* green   */
	[11] = "#ffff55", /* yellow  */
	[12] = "#5555ff", /* blue    */
	[13] = "#ff55ff", /* magenta */
	[14] = "#55ffff", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#000000", /* background */
	[257] = "#bbbbbb", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
