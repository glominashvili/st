/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#4e4e4e", /* black   */
	[1] = "#ff6c60", /* red     */
	[2] = "#a8ff60", /* green   */
	[3] = "#ffffb6", /* yellow  */
	[4] = "#69cbfe", /* blue    */
	[5] = "#ff73Fd", /* magenta */
	[6] = "#c6c5fe", /* cyan    */
	[7] = "#eeeeee", /* white   */

	/* 8 bright colors */
	[8]  = "#7c7c7c", /* black   */
	[9]  = "#ffb6b0", /* red     */
	[10] = "#ceffac", /* green   */
	[11] = "#ffffcb", /* yellow  */
	[12] = "#b5dcfe", /* blue    */
	[13] = "#ff9cfe", /* magenta */
	[14] = "#dfdffe", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#000000", /* background */
	[257] = "#eeeeee", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
