/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#232323", /* black   */
	[1] = "#ff000f", /* red     */
	[2] = "#8ce10b", /* green   */
	[3] = "#ffb900", /* yellow  */
	[4] = "#008df8", /* blue    */
	[5] = "#6d43a6", /* magenta */
	[6] = "#00d8eb", /* cyan    */
	[7] = "#ffffff", /* white   */

	/* 8 bright colors */
	[8]  = "#444444", /* black   */
	[9]  = "#ff2740", /* red     */
	[10] = "#abe15b", /* green   */
	[11] = "#ffd242", /* yellow  */
	[12] = "#0092ff", /* blue    */
	[13] = "#9a5feb", /* magenta */
	[14] = "#67fff0", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#0e1019", /* background */
	[257] = "#fffaf4", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
